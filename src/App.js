import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";

function App() {
  const [sum, setSum] = useState(0);
  return (
    <div className="App">
      <button onClick={() => setSum((e) => e + 1)}>+1</button>
      <h1>{sum}</h1>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
